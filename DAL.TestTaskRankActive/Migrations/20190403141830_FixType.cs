﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.TestTaskRankActive.Migrations
{
    public partial class FixType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "TaskID",
                table: "RankRequests",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "TaskID",
                table: "RankRequests",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);
        }
    }
}
