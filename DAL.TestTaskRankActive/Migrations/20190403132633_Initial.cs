﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.TestTaskRankActive.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "RankRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Website = table.Column<string>(nullable: false),
                    Keyword = table.Column<string>(nullable: false),
                    Status = table.Column<int>(nullable: true),
                    SearchID = table.Column<int>(nullable: false),
                    SearchName = table.Column<string>(nullable: true),
                    LocationID = table.Column<int>(nullable: false),
                    LocationName = table.Column<string>(nullable: true),
                    TaskID = table.Column<int>(nullable: true),
                    Position = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RankRequests", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RankRequests");
        }
    }
}
