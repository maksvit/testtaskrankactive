﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.TestTaskRankActive.Models
{

    public class LocationsResponse
    {
        public string Status { get; set; }
        public Error Error { get; set; }
        public string Results_time { get; set; }
        public string Results_count { get; set; }
        public List<Locations> Results { get; set; }
    }
    public class Locations
    {
        public int Loc_id { get; set; }
        public int? Loc_id_parent { get; set; }
        public string Loc_name { get; set; }
        public string Loc_name_canonical { get; set; }
        public string Loc_type { get; set; }
        public string Loc_country_iso_code { get; set; }
        public bool Dma_region { get; set; }
        public bool Kwrd_finder { get; set; }
        public string Kwrd_finder_lang { get; set; }
    }


    public class LocationsView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
