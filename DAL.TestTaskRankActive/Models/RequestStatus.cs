﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.TestTaskRankActive.Models
{
    public enum RequestStatus {
        Error = 1,
        Processing = 2,
        Tracked = 3
    }
}
