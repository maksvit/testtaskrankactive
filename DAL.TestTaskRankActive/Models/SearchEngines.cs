﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.TestTaskRankActive.Models
{

    public class SearchEnginesResponse
    {
        public string Status { get; set; }
        public Error Error { get; set; }
        public string Results_time { get; set; }
        public string Results_count { get; set; }
        public List<SearchEngines> Results { get; set; }
    }
    public class SearchEngines
    {
        public int Se_id { get; set; }
        public string Se_name { get; set; }
        public string Se_country_name { get; set; }
        public string Se_country_iso_code { get; set; }
        public string Se_language { get; set; }
        public string Se_localization { get; set; }
    }
    public class SearchEnginesView {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}
