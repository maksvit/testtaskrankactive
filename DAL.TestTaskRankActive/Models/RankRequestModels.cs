﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.TestTaskRankActive.Models
{
    public class RankRequestPost
    {
        [Required(AllowEmptyStrings = false)]
        public string Website { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Keyword { get; set; }
        [Required]
        public int SearchID { get; set; }
        public string SearchName { get; set; }
        [Required]
        public int LocationID { get; set; }
        public string LocationName { get; set; }
    }


    public class RankResponse
    {
        public string Status { get; set; }
        public Error Error { get; set; }
        public string Results_time { get; set; }
        public string Results_count { get; set; }
        public List<Rank> Results { get; set; }
    }


    public class Rank
    {
        public long Task_id { get; set; }
        public string Status { get; set; }
        public List<Error> Error { get; set; }
        public string Post_id { get; set; }
        public string Post_site { get; set; }
        public string Post_key { get; set; }
        public int Se_id { get; set; }
        public int Loc_id { get; set; }
        public int Key_id { get; set; }
    }


    public class RankRequestPostBack {
        public string Status { get; set; }
        public Error Error { get; set; }
        public string Results_time { get; set; }
        public string Results_count { get; set; }
        public dynamic Results { get; set; }
    }

    public class RankPostbackResults
    {
        public List<RankPostback> Organic { get; set; }
    }

    public class RankPostback
    {
        public long Task_id { get; set; }
        public int Post_id { get; set; }
        public string Post_site { get; set; }
        public string Post_key { get; set; }
        public int Se_id { get; set; }
        public int Loc_id { get; set; }
        public int Key_id { get; set; }
        public int? Result_position { get; set; }

    }


    public class RankRequestView
    {
        public int Id { get; set; }
        public string Website { get; set; }
        public string Keyword { get; set; }
        public string Status { get; set; }
        public int SearchID { get; set; }
        public string SearchName { get; set; }
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public long? TaskID { get; set; }
        public string Position { get; set; }
    }
}
