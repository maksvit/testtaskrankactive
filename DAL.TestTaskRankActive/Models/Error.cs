﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.TestTaskRankActive.Models
{
    public class Error
    {
        public int Code { get; set; }
        public string Message { get; set; }
    }
}
