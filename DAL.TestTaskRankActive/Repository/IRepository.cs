﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DAL.TestTaskRankActive.Repository
{
    public interface IRepository<T> where T : class
    {

        ICollection<T> GetAll();

        T GetById(int? id);


        T Insert(T entity);


        T Update(T updated);

        void Delete(T t);

        IList<T> GetAllInclude(params Expression<Func<T, object>>[] navigationProperties);

    }
}