﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DAL.TestTaskRankActive.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {

        private readonly ApplicationDbContext _context;
        public string ErrorMessage { get; set; } = string.Empty;
        private readonly IUnitOfWork _unitOfWork;

        public Repository(ApplicationDbContext context)
        {
            _context = context;
            _unitOfWork = new UnitOfWork(context);
        }



        public void Delete(T t)
        {
            if (_context.Set<T>() == null)
            {
                ErrorMessage = "The item you trying to Delete is not in Database anymore";
                throw new ArgumentNullException(nameof(t));
            }
            _context.Set<T>().Remove(t);
            _context.SaveChanges();
        }



        public ICollection<T> GetAll()
        {
            return _context.Set<T>().ToList();
        }



        public IList<T> GetAllInclude(params Expression<Func<T, object>>[] navigationProperties)
        {
            IQueryable<T> dbQuery = _context.Set<T>();

            //Apply eager loading
            foreach (Expression<Func<T, object>> navigationProperty in navigationProperties)
                dbQuery = dbQuery.Include<T, object>(navigationProperty);

            var list = dbQuery
                .AsNoTracking()
                .ToList();

            return list;
        }



        /// <summary>
        /// returns list of cattle by firm
        /// </summary>
        public T GetById(int? id)
        {
            return _context.Set<T>().Find(id);
        }





        public T Insert(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }


        public T Update(T updated)
        {
            if (_context.Set<T>() == null)
            {
                ErrorMessage = "No Database detected";
                throw new ArgumentNullException(nameof(updated));
            }
            _context.Set<T>().Attach(updated);
            _context.Entry(updated).State = EntityState.Modified;
            _context.SaveChanges();
            return updated;
        }

    }
}
