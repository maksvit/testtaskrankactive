﻿using DAL.TestTaskRankActive.Tables;
using Microsoft.EntityFrameworkCore;

namespace DAL.TestTaskRankActive
{
    public class ApplicationDbContext:DbContext 
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            //Database.EnsureCreated();
        }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
        }
        public DbSet<RankRequest> RankRequests { get; set; }

    }
}
