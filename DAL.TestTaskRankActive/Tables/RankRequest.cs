﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.TestTaskRankActive.Tables
{
    public class RankRequest
    {
        public int Id { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Website { get; set; }
        [Required(AllowEmptyStrings = false)]
        public string Keyword { get; set; }
        public int? Status { get; set; }
        [Required]
        public int SearchID { get; set; }
        public string SearchName { get; set; }
        [Required]
        public int LocationID { get; set; }
        public string LocationName { get; set; }
        public long? TaskID { get; set; }
        public int? Position { get; set; }
    }
}
