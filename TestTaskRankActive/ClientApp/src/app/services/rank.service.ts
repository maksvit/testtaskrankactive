import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})

export class RankService {
  baseUrl: string = "api/1.0/rank";
  constructor(private http: HttpClient) {

  }



  getLocations() {
    return this.http.get<Locations[]>(this.baseUrl + "/locations");
  }

  getSearchEngines() {
    return this.http.get<SearchEngines[]>(this.baseUrl + "/searchengines");
  }
  rankRequest(data:any) {
    return this.http.post<RankRequestResponse>(this.baseUrl + "/rankrequest", data);
  }

  getRankResults() {
  
    return this.http.get<RankRequest[]>(this.baseUrl);
  }
}
