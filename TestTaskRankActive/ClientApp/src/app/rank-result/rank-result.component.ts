import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RankService } from "../services/rank.service";

@Component({
  selector: 'app-rank-result',
  templateUrl: './rank-result.component.html'
})
export class RankResultComponent {
  public rankResults: RankRequest[];

  constructor(private rankService: RankService) {
    rankService.getRankResults().subscribe(result => {
      this.rankResults = result;
    }, error => console.error(error));
  }
}

