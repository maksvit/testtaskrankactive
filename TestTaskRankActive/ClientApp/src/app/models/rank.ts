interface Locations {
  id: number;
  name: string;
}

interface SearchEngines {
  id: number;
  name: string;
  country: string;
}


interface RankRequestResponse {
  id: number;
}

interface RankRequest {
  id: number;
  searchID: number;
  searchName: string;
  locationID: number;
  locationName: string;
  keyword: string;
  website: string;
  status: string;
  taskID: number;
}


