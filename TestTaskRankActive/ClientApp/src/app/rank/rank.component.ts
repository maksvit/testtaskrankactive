import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators  } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { RankService } from "../services/rank.service";

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
})
export class RankComponent implements OnInit {
  rankForm: FormGroup;
  submitted = false;
  public locations: Locations[];
  public searchEngines: SearchEngines[];
  constructor(private formBuilder: FormBuilder, http: HttpClient,  private rankService: RankService) {
    rankService.getLocations().subscribe(result => {
      this.locations = result;
    }, error => console.error(error));
    rankService.getSearchEngines().subscribe(result => {
      this.searchEngines = result;
    }, error => console.error(error));
  }

  ngOnInit() {

    const reg = '(https?://)?([\\da-z.-]+)\\.([a-z.]{2,6})[/\\w .-]*/?';
    this.rankForm = this.formBuilder.group({
      searchEngine: ['', Validators.required],
      location: ['', Validators.required],
      website: ['', [Validators.required, Validators.pattern(reg)]],
      keyword: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.rankForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.rankForm.invalid) {
      return;
    }
    var slctLocationID = parseInt(this.rankForm.controls["location"].value);
    var slctLocation = this.locations.find(x => x.id === slctLocationID);
    var slctSearchEngineID = parseInt(this.rankForm.controls["searchEngine"].value);
    var slctSearchEngine = this.searchEngines.find(x => x.id === slctSearchEngineID);
    var SearchEngineName = slctSearchEngine.name + " - " + slctSearchEngine.country;
    var website = this.rankForm.controls["website"].value;
    var keyword = this.rankForm.controls["keyword"].value;
    var data = {
      searchID: slctSearchEngine.id,
      searchName: SearchEngineName,
      locationID: slctLocation.id,
      locationName: slctLocation.name,
      keyword: keyword,
      website: website,
    };
    this.rankService.rankRequest(data).subscribe(result => {
      alert("Send success");
      this.rankForm.reset();
    }, error => {
      alert(error.message);
      console.error(error.message);
    });

  }

 
}


