﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.TestTaskRankActive.Interfaces;
using BLL.TestTaskRankActive.RankApi;
using DAL.TestTaskRankActive.Models;
using DAL.TestTaskRankActive.Tables;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestTaskRankActive.Controllers
{
    [ApiVersion("1.0")]
    [Route("api/{version:apiVersion}/[controller]")]
    [ApiController]
    public class RankController : Controller
    {
        private readonly IRankService _rankService;

        public RankController(IRankService rankService)
        {
            _rankService = rankService;
        }

        [HttpGet]
        [Route("locations")]
        [ProducesResponseType(200, Type = typeof(List<LocationsView>))]
        public async Task<List<LocationsView>> GetLocations()
        {
            return await RankApi.GetLocations();
        }


        [HttpGet]
        [Route("searchengines")]
        [ProducesResponseType(200, Type = typeof(List<SearchEnginesView>))]
        public async Task<List<SearchEnginesView>> GetSearchEngines()
        {
            return await RankApi.GetSearchEngines();
        }


        [HttpPost]
        [Route("rankrequest")]
        [ProducesResponseType(200, Type = typeof(List<RankRequestPost>))]
        public ActionResult Post([FromBody]RankRequestPost data)
        {
            var result = _rankService.GetRank(data);
            if (!result) return BadRequest();
            return Ok();
        }


        [HttpPost]
        [Route("rankpostback")]
        [ProducesResponseType(200, Type = typeof(List<RankRequestPost>))]
        public ActionResult Post([FromBody]RankRequestPostBack data)
        {
            var result = _rankService.RankPostBack(data);
            return Ok();
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<RankRequestView>))]
        public List<RankRequestView> GetAll()
        {
            var result = _rankService.GetAllRank();
            return result;
        }
    }
}
