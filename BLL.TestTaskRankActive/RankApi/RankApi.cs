﻿using DAL.TestTaskRankActive.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace BLL.TestTaskRankActive.RankApi
{
    public class RankApi
    {
        private static readonly string USER = "challenger30@rankactive.info";
        private static readonly string PASS = "pT8waU8ss";

        public static async Task<List<LocationsView>> GetLocations()
        {
            var result = new List<LocationsView>();
            var httpClient = GetHttpClient();
            var response = await httpClient.GetAsync("v2/cmn_locations/nz");
            var obj = JsonConvert.DeserializeObject<LocationsResponse>(await response.Content.ReadAsStringAsync());
            if (obj.Status == "error")
                Console.WriteLine($"error. Code: {obj.Error.Code} Message: {obj.Error.Message}");
            else
            {
                result = obj.Results.Select(x=> new LocationsView {Id = x.Loc_id, Name = x.Loc_name}).ToList();
            }

            return result;
        }


        public static async Task<List<SearchEnginesView>> GetSearchEngines()
        {
            var result = new List<SearchEnginesView>();
            var httpClient = GetHttpClient();
            var response = await httpClient.GetAsync("v2/cmn_se");
            var obj = JsonConvert.DeserializeObject<SearchEnginesResponse>(await response.Content.ReadAsStringAsync());
            if (obj.Status == "error")
                Console.WriteLine($"error. Code: {obj.Error.Code} Message: {obj.Error.Message}");
            else
            {
                result = obj.Results.Select(x => new SearchEnginesView { Id = x.Se_id, Name = x.Se_name, Country = x.Se_country_name }).ToList();
            }

            return result;
        }


        public static Rank RankPost(Dictionary<int, object> postObject)
        {
            Rank result = null;
            var httpClient = GetHttpClient();

            var taskPostResponse = httpClient.PostAsync("v2/rnk_tasks_post", new StringContent(JsonConvert.SerializeObject(new { data = postObject }))).Result;
            var resp = taskPostResponse.Content.ReadAsStringAsync().Result;
            var obj = JsonConvert.DeserializeObject<dynamic>(resp);
            if (obj.status == "error")
                Console.WriteLine($"error. Code: {obj.Error.Code} Message: {obj.Error.Message}");
            else
            {
                var listResult = new List<Rank>();
                foreach (var res in obj.results)
                {
                    var resultItem = ((IEnumerable<dynamic>)res).First();

                    var rank =resultItem.ToObject<Rank>();
                    listResult.Add(rank);
                }
                result = listResult.FirstOrDefault();
            }
            return result;
        }


        private static HttpClient GetHttpClient()
        {
            var httpClient = new HttpClient
            {
                BaseAddress = new Uri("https://api.dataforseo.com/"),

                //Instead of 'login' and 'password' use your credentials from https://my.dataforseo.com/login
                DefaultRequestHeaders = { Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(Encoding.ASCII.GetBytes(USER+":"+PASS))) }
            };
            return httpClient;
        }
    }
}
