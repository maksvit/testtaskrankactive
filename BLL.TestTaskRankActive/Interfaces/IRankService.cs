﻿using DAL.TestTaskRankActive.Models;
using DAL.TestTaskRankActive.Tables;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.TestTaskRankActive.Interfaces
{
    public interface IRankService
    {
        bool GetRank(RankRequestPost data);
        bool RankPostBack(RankRequestPostBack data);
        List<RankRequestView> GetAllRank();
    }
}
