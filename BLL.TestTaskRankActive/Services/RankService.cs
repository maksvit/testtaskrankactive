﻿using BLL.TestTaskRankActive.Interfaces;
using DAL.TestTaskRankActive.Models;
using DAL.TestTaskRankActive.Repository;
using DAL.TestTaskRankActive.Tables;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BLL.TestTaskRankActive.Services
{
    public class RankService : IRankService
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly string POSTBACK_URL = "http://maksvitkh-001-site1.etempurl.com/api/1.0/rank/rankpostback";
        public RankService(ILogger logger, IUnitOfWork unitOfWork) {
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public bool GetRank(RankRequestPost data)
        {
            var result = false;

            try
            {
                _logger.ForContext("Method", "GetRank").Verbose("GetRank.Post:{@data}", data);

                var newRank = new RankRequest()
                {
                    Website = data.Website,
                    Keyword = data.Keyword,
                    SearchID = data.SearchID,
                    SearchName = data.SearchName,
                    LocationID = data.LocationID,
                    LocationName = data.LocationName
                };

                 _unitOfWork.Repository<RankRequest>().Insert(newRank);


                    if (newRank.Id > 0) {
                    var post = new Dictionary<int, object>
                    {
                        [newRank.Id] = new
                        {
                            site = newRank.Website,
                            se_id = newRank.SearchID,
                            loc_id = newRank.LocationID,
                            key = newRank.Keyword,
                            postback_url = POSTBACK_URL
                        }
                    };
                    var response =  RankApi.RankApi.RankPost(post);
                    if (response.Status == "error")
                    {
                       // _logger.ForContext("Method", "GetRank").Error($"Error in task with post_id {response.Post_id}. Code: {response.Error.Code} Message: {response.Error.Message} " + "Response:{@response}", response);
                        newRank.Status = (int)RequestStatus.Error;
                    }
                    else
                    {
                        _logger.ForContext("Method", "GetRank").Information("GetRank. Success. Response:{ @response}", response);
                        newRank.Status = (int)RequestStatus.Processing;
                        newRank.TaskID = response.Task_id;
                    }
                    _unitOfWork.Repository<RankRequest>().Update(newRank);
                }

                if(newRank.Status == (int)RequestStatus.Processing)
                {
                    result = true;
                }

                _logger.ForContext("Method", "GetRank").Information("GetRank. Success.");
            }
            catch (Exception ex)
            {
                _logger.ForContext("Method", "GetRank").Error(ex, "GetRank.Post:{@data}.", data);
            }

            return result;
        }

        public bool RankPostBack(RankRequestPostBack data)
        {
            var result = false;
            try
            {
                _logger.ForContext("Method", "RankPostBackAsync").Verbose("RankPostBackAsync.Post:{@data}", data);

                var obj = data;
                if (obj.Status == "error")
                    _logger.ForContext("Method", "RankPostBackAsync").Error("RankPostBackAsync.Post:{@data}.", data);
                else if (Int32.Parse(obj.Results_count) != 0)
                {
                    var listResult = new List<RankPostback>();
                    foreach (var resObj in obj.Results)
                    {
                        var resultItem = ((IEnumerable<dynamic>)resObj).First();

                        var rank = resultItem.ToObject< List<RankPostback>>();
                        listResult.AddRange(rank);
                    }
                    var responseResult = listResult.FirstOrDefault();


                        if (responseResult != null)
                        {
                            var rank = _unitOfWork.Repository<RankRequest>().GetById(responseResult.Post_id);
                            if(rank != null)
                            {
                                if (obj.Status == "error")
                                {
                                    rank.Status = (int)RequestStatus.Error;
                                }
                                else
                                {
                                    rank.Status = (int)RequestStatus.Tracked;
                                    rank.Position = responseResult.Result_position;
                                }
                                _unitOfWork.Repository<RankRequest>().Update(rank);

                            }
                    }
                   
                }
                else
                    Console.WriteLine("no results");
                result = true;
            }
            catch (Exception ex)
            {
                _logger.ForContext("Method", "RankPostBackAsync").Error(ex, "RankPostBackAsync.Post:{@data}.", data);
            }
            return result;
        }

        public List<RankRequestView> GetAllRank()
        {
            var result = new List<RankRequestView>();
            try
            {
                result = _unitOfWork.Repository<RankRequest>().GetAll().Where(x => x.Status.HasValue && x.Status != (int)RequestStatus.Error).OrderByDescending(x=>x.Id)
                    .Select(x=> new RankRequestView {
                        Id = x.Id,
                        Keyword = x.Keyword,
                        LocationID = x.LocationID,
                        LocationName = x.LocationName,
                        Position = x.Position.HasValue? x.Position.ToString():"",
                        SearchID = x.SearchID,
                        SearchName = x.SearchName,
                        TaskID = x.TaskID,
                        Website = x.Website,
                        Status = Enum.GetName(typeof(RequestStatus), x.Status)
                    }).ToList();
            }
            catch (Exception ex)
            {
                _logger.ForContext("Method", "GetAllRank").Error(ex, "GetAllRank");
            }
            return result;
        }

    }
}
